const { Router } = require("express");
const { MongoClient, ObjectId } = require("mongodb");

if (process.env.MODE === "production") {
  url = `mongodb://tysm:${process.env.MONGO_PASSWORD}@192.168.171.67`;
} else {
  url = `mongodb://127.0.0.1:8102`;
}
console.log(url);
const mongoClient = new MongoClient(url);
const db = mongoClient.db(
  process.env.MODE === "production" ? "tysm" : "blackjack" //if production, connect to my db, otherwise blackjack
);

const apiRouter = Router();

apiRouter.post("/user/", async (req, res) => {
  //put data into server with post (CREATION)
  console.log("debug");
  try {
    const users = db.collection("UserData");
    delete req.body.password;
    const newUser = await users.insertOne({
      ...req.body, //... is a spreader to spread parameters into newly created object
    });
    res.json(newUser);
  } catch (err) {
    console.error(err);
    res.sendStatus(500); //500 is a server-side error
  }
});

apiRouter.get("/user/:id", async (req, res) => {
  //(ACCESS)
  try {
    const users = db.collection("UserData");
    const searchedUser = await users.findOne({
      _id: new ObjectId(req.params.id),
    });
    res.json(searchedUser);
  } catch (err) {
    console.error(err);
    res.sendStatus(500);
  }
});
apiRouter.get("/hello", async (req, res) => {
  //(ACCESS)
  try {
    console.log("hello world");
  } catch (err) {
    console.error(err);
    res.sendStatus(500);
  }
});

apiRouter.patch("/user/:id", async (req, res) => {
  //(UPDATION)
  try {
    const users = db.collection("UserData");
    if (req.body.username) {
      const username = req.body.username;
      const updatedUser = await users.updateOne(
        {
          _id: new ObjectId(req.params.id),
        },
        {
          $set: { username }, //shorthand for { "username": username}
        }
      );
      res.json(updatedUser);
    } else {
      res.sendStatus(400); //client error
    }
  } catch (err) {
    console.error(err);
    res.sendStatus(500);
  }
});

apiRouter.delete("/user/:id", async (req, res) => {
  //(DELETION)
  try {
    const users = db.collection("UserData");
    const deletedUser = await users.deleteOne({
      _id: ObjectId(req.params.id),
    });
    res.json(deletedUser);
  } catch (err) {
    console.error(err);
    res.sendStatus(500);
  }
});
module.exports = apiRouter;
