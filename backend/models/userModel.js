//create mongoose data model

import mongoose from "mongoose";

const userSchema = new mongoose.Schema(
  {
    name: { type: String, required: true },
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    isAdmin: { type: Boolean, default: false, require: true },
  },
  {
    timestamps: true, //timestamp will be created whenever a new product is inserted
  }
);

const User = mongoose.model("User", userSchema);
export default User;
