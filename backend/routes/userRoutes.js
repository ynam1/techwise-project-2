import express from 'express';
import User from '../models/userModel.js';
import bcrypt from 'bcryptjs';
import expressAsyncHandler from 'express-async-handler';
import { isAuth, generateToken } from '../utils.js';

const userRouter = express.Router();

userRouter.post(
  //this function helps us to catch the async errors insdie
  '/signin',
  expressAsyncHandler(async (req, res) => {
    //implement function to signin
    const user = await User.findOne({ email: req.body.email });
    if (user) {
      //since password is encrypted, we have to compare the bcrypt password.
      //req.body.password is the plain text password and the user.password is the ncrypted password
      if (bcrypt.compareSync(req.body.password, user.password)) {
        //password checking
        res.send({
          _id: user._id,
          name: user.name,
          email: user.email,
          isAdmin: user.isAdmin,
          //user token gives authenticated permissions or actions
          token: generateToken(user),
        });
        return;
      }
    }
    //This just returns if the email or username is wrong
    res.status(401).send({ message: 'Invalid email or password' });
  })
);

userRouter.post(
  '/signup',
  expressAsyncHandler(async (req, res) => {
    //We created a new instance of a user that is just signing up
    const newUser = new User({
      name: req.body.name,
      email: req.body.email,
      //Use hash to encrypt the password
      password: bcrypt.hashSync(req.body.password),
    });
    //we use await to store to the database
    const user = await newUser.save();
    //return to the front end
    res.send({
      _id: user._id,
      name: user.name,
      email: user.email,
      isAdmin: user.isAdmin,
      //user token gives authenticated permissions or actions
      token: generateToken(user),
    });
  })
);

userRouter.put(
  '/profile',
  isAuth,
  expressAsyncHandler(async (req, res) => {
    const user = await User.findById(req.user._id);
    if (user) {
      user.name = req.body.name || user.name;
      user.email = req.body.email || user.email;

      if (req.body.password) {
        user.password = bcrypt.hashSync(req.body.password, 8);
      }
      const updatedUser = await user.save();
      res.send({
        _id: updatedUser._id,
        name: updatedUser.name,
        email: updatedUser.email,
        isAdmin: updatedUser.isAdmin,
        token: generateToken(updatedUser),
      });
    } else {
      res.status(404).send({ message: 'User not found' });
    }
  })
);

export default userRouter;
