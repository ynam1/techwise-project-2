import express from "express";
import Product from "../models/productModel.js";
import data from "../data.js";
import User from "../models/userModel.js";

const seedRouter = express.Router();

//shift-alt-down is to duplicate a line.
//press alt to move line around

seedRouter.get("/", async (req, res) => {
  await Product.remove({});
  const createdProducts = await Product.insertMany(data.products); //insert array of products into database
  await User.remove({});
  //we have to crete users in data.js
  const createdUsers = await User.insertMany(data.users); //insert array of products into database
  res.send({ createdProducts, createdUsers }); //send back products to frontend
});

export default seedRouter;
