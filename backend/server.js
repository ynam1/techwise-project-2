import express from "express";
import mongoose from "mongoose";
import dotenv from "dotenv";
import seedRouter from "./routes/seedRoutes.js";
import productRouter from "./routes/productRoutes.js";
import userRouter from "./routes/userRoutes.js";
import orderRouter from "./routes/orderRoutes.js";
import path from "path";
import morgan from "morgan";

dotenv.config();

//coming from .env file
mongoose
  .connect(process.env.MONGODB_URI)
  .then(() => {
    console.log("connected to db good job boi");
  })
  .catch((err) => {
    console.log(err.message);
  });
//optional
// mongoose.set('strictQuery', false);

const app = express();

app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  }),
  morgan("dev")
);

//created middleware
const prefixRouter = express.Router();

prefixRouter.use("/api/seed", seedRouter);
prefixRouter.use("/api/products", productRouter);
prefixRouter.use("/api/users", userRouter);
prefixRouter.use("/api/orders", orderRouter);
const __dirname = path.resolve();
prefixRouter.use(express.static(path.join(__dirname, "../my-app/build")));
prefixRouter.use(express.static(path.join(__dirname, "../my-app/build")));
prefixRouter.get("*", (req, res) =>
  res.sendFile(path.join(__dirname, "../my-app/build/index.html"))
);
if (process.env.MODE == "development") {
  app.use("/tysm/", prefixRouter);
} else {
  app.use("/", prefixRouter);
}

// app.use("/api/seed", seedRouter);
// app.use("/api/products", productRouter);
// app.use("/api/users", userRouter);
// app.use("/api/orders", orderRouter);
//test
// const __dirname = path.resolve();
// app.use(express.static(path.join(__dirname, "../my-app/build")));
// app.get("*", (req, res) =>
//   res.sendFile(path.join(__dirname, "../my-app/build/index.html"))
// );
//define error handler for express
app.use((err, req, res, next) => {
  //this is a middleware
  //500 is error in server
  res.status(500).send({ message: err.message });
});

const port = process.env.PORT || 8102;
app.listen(port, () => {
  console.log(`serve at http://localhost:${port}`);
});
