import { Link } from 'react-router-dom';
import Badge from 'react-bootstrap/Badge';
import { ToastContainer } from 'react-toastify';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Container from 'react-bootstrap/Container';
import { LinkContainer } from 'react-router-bootstrap';
import { useContext, useState } from 'react';
import { Store } from './Store';
import SearchBox from './components/SearchBox';

function NavBar() {
  const { state, dispatch: ctxDispatch } = useContext(Store);
  const { cart, userInfo } = state;

  const [sidebarIsOpen, setSidebarIsOpen] = useState(false);

  const signoutHandler = () => {
    ctxDispatch({ type: 'USER_SIGNOUT' });
    localStorage.removeItem('userInfo');
    localStorage.removeItem('shippingAddress');
    localStorage.removeItem('paymentMethod');
    window.location.href = '/tysm/signin';
  };
  return (
    <div>
      <Link to="/">
        <img class="home-logo" src="./images/logo_main.png" alt="tysm_logo" />
      </Link>
      <SearchBox></SearchBox>
      <nav class="other-buttons-container">
        <Link to="/cart" title="Cart">
          <img class="cart-img" src="./images/Cart.png" alt="cart" />
          {cart.cartItems.length > 0 && (
            <Badge pill bg="danger">
              {cart.cartItems.reduce((a, c) => a + c.quantity, 0)}
            </Badge>
          )}
        </Link>
        <Link to="/profile">
          <img class="account-img" src="./images/Account.png" alt="account" />
        </Link>
        <Link to="/orderhistory">
          <img class="wish-img" src="./images/wishlist2.png" alt="wishlist" />
        </Link>
      </nav>
      <section class="login-signup-container">
        {userInfo ? (
          <NavDropdown title={userInfo.name} id="basic-nav-dropdown">
            {/* <LinkContainer to="/profile">
              <NavDropdown.Item>User Profile</NavDropdown.Item>
            </LinkContainer>
            <LinkContainer to="/orderhistory">
              <NavDropdown.Item>Order History</NavDropdown.Item>
            </LinkContainer> */}
            <NavDropdown.Divider />
            <Link
              className="dropdown-item"
              to="#signout"
              onClick={signoutHandler}
            >
              Sign Out
            </Link>
          </NavDropdown>
        ) : (
          <section>
            <Link to="/signin">
              <button class="login-signup-button">Login</button>
            </Link>

            <Link to="/signup">
              <button class="login-signup-button">Signup</button>
            </Link>
          </section>
        )}
        {userInfo && userInfo.isAdmin && (
          <NavDropdown title="Admin" id="admin-nav-dropdown">
            <LinkContainer to="/admin/dashboard">
              <NavDropdown.Item>Dashboard</NavDropdown.Item>
            </LinkContainer>
            <LinkContainer to="/admin/productlist">
              <NavDropdown.Item>Products</NavDropdown.Item>
            </LinkContainer>
            <LinkContainer to="/admin/orderlist">
              <NavDropdown.Item>Orders</NavDropdown.Item>
            </LinkContainer>
            <LinkContainer to="/admin/userlist">
              <NavDropdown.Item>Users</NavDropdown.Item>
            </LinkContainer>
          </NavDropdown>
        )}
      </section>
      <nav class="home-search">
        <Link to="/">
          <button class="main-nav-button">
            <img class="home-img" src="./images/WishList.png" alt="home" />
            Home
          </button>
        </Link>
        <Link to="/search">
          <button class="main-nav-button">
            <img class="home-img" src="./images/Search.png" alt="shop" />
            Search
          </button>
        </Link>
      </nav>
    </div>
  );
}
export default NavBar;
