import axios from "axios";
import React, { useReducer } from "react";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import ListGroup from "react-bootstrap/ListGroup";
import CheckoutSteps from "../components/CheckoutSteps";
import { Link, useNavigate } from "react-router-dom";
import { useContext, useEffect, useState } from "react";
import { toast } from "react-toastify";
import { getError } from "../utils";
import { Store } from "../Store";
import LoadingBox from "../components/LoadingBox";
import ListGroupItem from "react-bootstrap/esm/ListGroupItem";

const reducer = (state, action) => {
  switch (action.type) {
    case "CREATE_REQUEST":
      return { ...state, loading: true };
    case "CREATE_SUCCESS":
      return { ...state, loading: false };
    case "CREATE_FAIL":
      return { ...state, loading: false };
    default:
      return state;
  }
};

export default function PlaceOrderScreen() {
  const navigate = useNavigate();

  const [{ loading }, dispatch] = useReducer(reducer, {
    loading: false,
  });

  const { state, dispatch: ctxDispatch } = useContext(Store);
  const { cart, userInfo } = state;

  const round2 = (num) => Math.round(num * 100 + Number.EPSILON) / 100; //rounds the number to two decimal spots
  cart.itemsPrice = round2(
    cart.cartItems.reduce((a, c) => a + c.quantity * c.price, 0) //finds the total price
  );
  cart.shippingPrice = cart.itemsPrice > 100 ? round2(0) : round2(10);
  cart.taxPrice = round2(0.15 * cart.itemsPrice);
  cart.totalPrice = cart.itemsPrice + cart.shippingPrice + cart.taxPrice;

  const placeOrderHandler = async () => {
    try {
      dispatch({ type: "CREATE_REQUEST" });

      const { data } = await axios.post(
        "/tysm/api/orders",
        {
          orderItems: cart.cartItems,
          shippingAddress: cart.shippingAddress,
          paymentMethod: cart.paymentMethod,
          itemsPrice: cart.itemsPrice,
          shippingPrice: cart.shippingPrice,
          taxPrice: cart.taxPrice,
          totalPrice: cart.totalPrice,
        },
        {
          headers: {
            authorization: `Bearer ${userInfo.token}`,
          },
        }
      );
      ctxDispatch({ type: "CART_CLEAR" });
      dispatch({ type: "CREATE_SUCCESS" });
      localStorage.removeItem("cartItems");
      navigate(`/order/${data.order._id}`);
    } catch (err) {
      dispatch({ type: "CREATE_FAIL" });
      toast.error(getError(err));
    }
  };

  useEffect(() => {
    if (!cart.paymentMethod) {
      navigate("/payment");
    }
  }, [cart, navigate]);
  return (
    <div>
      <CheckoutSteps step1 step2 step3 step4>
        {" "}
      </CheckoutSteps>
      <h1>
        <title>Preview Order </title>
      </h1>
      <h2 className="my-3">Preview Order</h2>
      <div class="container">
        <Card className="mb-3">
          <Card.Body>
            <Card.Title>Shipping</Card.Title>
            <Card.Text>
              <strong>Name:</strong> {cart.shippingAddress.fullName} <br />
              <strong>Address:</strong> {cart.shippingAddress.address},
              {cart.shippingAddress.city},{cart.shippingAddress.postalCode},
              {cart.shippingAddress.country}
            </Card.Text>
            <Link to="/shipping"> Edit </Link>
          </Card.Body>
        </Card>
        <Card className="mb-3">
          <Card.Body>
            <Card.Title>Payment</Card.Title>
            <Card.Text>
              <strong>Method:</strong> {cart.paymentMethod}
            </Card.Text>
            <Link to="/payment"> Edit </Link>
          </Card.Body>
        </Card>
        <Card className="mb-3">
          <Card.Body>
            <Card.Title>Items</Card.Title>
            <ListGroup variant="flush">
              {cart.cartItems.map(
                (
                  item //convert each item to listgroup item
                ) => (
                  <ListGroup.Item key={item._id}>
                    <div className="align-items-center">
                      <div md={6}>
                        <img
                          src={item.image}
                          alt={item.name}
                          className="img-fluid rounded img-thumbnail"
                        ></img>{" "}
                        <Link to={`/product/${item.slug}`}>{item.name}</Link>
                      </div>
                      <div md={3}>
                        <span>{item.quantity}</span>
                      </div>
                      <div md={3}>${item.price}</div>
                    </div>
                  </ListGroup.Item>
                )
              )}
            </ListGroup>
            <Link to="/cart">Edit</Link>
          </Card.Body>
        </Card>
      </div>
      <div md={4}>
        <Card.Body>
          <Card.Title> Order Summary</Card.Title>
          <ListGroup variant="flush">
            <ListGroup.Item>
              <div>
                <div>Items</div>
                <div>${cart.itemsPrice.toFixed(2)}</div>
              </div>
            </ListGroup.Item>
            <ListGroup.Item>
              <div>
                <div>Tax</div>
                <div>${cart.taxPrice.toFixed(2)}</div>
              </div>
            </ListGroup.Item>
            <ListGroup.Item>
              <div>
                <div>Order Total</div>
                <div>${cart.totalPrice.toFixed(2)}</div>
              </div>
            </ListGroup.Item>
            <ListGroup.Item>
              <div className="d-grid">
                <Button
                  type="button"
                  onClick={placeOrderHandler}
                  disabled={cart.cartItems.length === 0}
                >
                  Place Order
                </Button>
                <div>Shipping</div>
                <div>${cart.shippingPrice.toFixed(2)}</div>
              </div>
              {loading && <LoadingBox></LoadingBox>}
            </ListGroup.Item>
            <ListGroup.Item>
              <div>
                <div>Shipping</div>
                <div>${cart.shippingPrice.toFixed(2)}</div>
              </div>
            </ListGroup.Item>
          </ListGroup>
        </Card.Body>
      </div>
    </div>
  );
}
