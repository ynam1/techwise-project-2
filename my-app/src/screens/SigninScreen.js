import { Link, useLocation, useNavigate } from "react-router-dom";
import Button from "react-bootstrap/Button";
import "./SigninScreen.css";
import Axios from "axios";
import { useContext, useEffect, useState } from "react";
import { Store } from "../Store";
import { toast } from "react-toastify";
import { getError } from "../utils";

export default function SigninScreen() {
  const navigate = useNavigate();
  //get redirect value. use Location is a hook from react router dom
  const { search } = useLocation();
  const redirectInUrl = new URLSearchParams(search).get("redirect");
  const redirect = redirectInUrl ? redirectInUrl : "/";

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const { state, dispatch: ctxDispatch } = useContext(Store);
  const { userInfo } = state;
  const submitHandler = async (e) => {
    e.preventDefault();
    try {
      console.log("Here");

      const { data } = await Axios.post("/tysm/api/users/signin", {
        email,
        password,
      });
      ctxDispatch({ type: "USER_SIGNIN", payload: data });
      localStorage.setItem("userInfo", JSON.stringify(data));
      navigate(redirect || "/");
    } catch (err) {
      toast.error(getError(err));
    }
  };

  useEffect(() => {
    if (userInfo) {
      navigate(redirect);
    }
  }, [navigate, redirect, userInfo]);

  return (
    //Make the login form here
    <body>
      <form class="Login-form" onSubmit={submitHandler}>
        <h1 className="my-3">Log In</h1>
        <div class="username-container">
          <label class="username-label">Email</label>
          <input
            class="form-input"
            id="txt-input"
            type="text"
            placeholder="Ex: johnsmith@gmail.com"
            required
            onChange={(e) => setEmail(e.target.value)}
          ></input>
        </div>
        <div class="password-container">
          <label>Password</label>
          <input
            class="form-input"
            id="txt-input"
            type="password"
            placeholder="Ex: IloveCats1120"
            required
            onChange={(e) => setPassword(e.target.value)}
          ></input>
        </div>
        <div class="login-container">
          <Button type="submit">Log In</Button>
        </div>
        <div class="redirect-user">
          New User?{" "}
          <Link class="redirect-link" to={`/signup?redirect=${redirect}`}>
            Create a new account
          </Link>
        </div>
      </form>
    </body>
  );
}
