import { Link, useLocation, useNavigate } from "react-router-dom";
import Button from "react-bootstrap/Button";
import "./SigninScreen.css";
import Axios from "axios";
import { useContext, useEffect, useState } from "react";
import { Store } from "../Store";
import { toast } from "react-toastify";
import { getError } from "../utils";
import "./SignupScreen.css";
export default function SignupScreen() {
  const navigate = useNavigate();
  //get redirect value. use Location is a hook from react router dom
  const { search } = useLocation();
  const redirectInUrl = new URLSearchParams(search).get("redirect");
  const redirect = redirectInUrl ? redirectInUrl : "/";
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");

  const { state, dispatch: ctxDispatch } = useContext(Store);
  const { userInfo } = state;
  const submitHandler = async (e) => {
    e.preventDefault();
    //this if statement is to compare the password and confirm password
    if (password !== confirmPassword) {
      toast.error("Passwords do not match");
      return;
    }

    try {
      console.log("Here");

      const { data } = await Axios.post("/tysm/api/users/signup", {
        name,
        email,
        password,
      });
      ctxDispatch({ type: "USER_SIGNIN", payload: data });
      localStorage.setItem("userInfo", JSON.stringify(data));
      navigate(redirect || "/");
    } catch (err) {
      toast.error(getError(err));
    }
  };

  useEffect(() => {
    if (userInfo) {
      navigate(redirect);
    }
  }, [navigate, redirect, userInfo]);

  return (
    //Make the login form here
    <body>
      <form class="Login-form" onSubmit={submitHandler}>
        <h1 className="my-3">Sign Up</h1>
        {/* This is for the name form */}
        <div class="name-container">
          <label class="name-label">Name</label>
          <input
            class="form-input"
            id="txt-input"
            type="text"
            placeholder="Ex: Sebastian"
            required
            onChange={(e) => setName(e.target.value)}
          ></input>
        </div>

        {/* This is for the mail form */}
        <div class="username-container">
          <label class="username-label">Email</label>
          <input
            class="form-input"
            id="txt-input"
            type="text"
            placeholder="Ex: johnsmith@gmail.com"
            required
            onChange={(e) => setEmail(e.target.value)}
          ></input>
        </div>
        <div class="password-container">
          <label>Password</label>
          <input
            class="form-input"
            id="txt-input"
            type="password"
            placeholder="Ex: IloveCats1120"
            required
            onChange={(e) => setPassword(e.target.value)}
          ></input>
        </div>

        {/* This section is for confirming the password */}

        <div class="confirmPass-container">
          <label class="confirmPass-label">Confirm Password</label>
          <input
            class="form-input"
            id="txt-input"
            type="password"
            required
            onChange={(e) => setConfirmPassword(e.target.value)}
          ></input>
        </div>
        <section class="login-container">
          <div class="sign-up">
            <Button type="submit">Sign Up</Button>
          </div>
          <div class="redirect-user">
            Already have an account?{" "}
            <Link class="redirect-link" to={`/signin?redirect=${redirect}`}>
              Log In
            </Link>
          </div>
        </section>
      </form>
    </body>
  );
}
