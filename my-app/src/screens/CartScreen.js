import { useContext } from "react";
import { Store } from "../Store";
import { Helmet } from "react-helmet-async";
import MessageBox from "../components/MessageBox";
import { Link, useNavigate } from "react-router-dom";
import ListGroup from "react-bootstrap/ListGroup";
import Row from "react-bootstrap/Row";
import Button from "react-bootstrap/Button";
import axios from "axios";
export default function CartScreen() {
  const navigate = useNavigate();
  const { state, dispatch: ctxDispatch } = useContext(Store);
  const {
    cart: { cartItems },
  } = state;

  const updateCartHandler = async (item, quantity) => {
    const { data } = await axios.get(`/tysm/api/products/${item._id}`);
    if (data.countInStock < quantity) {
      window.alert("Sorry. Product is out of stock");
      return;
    }
    ctxDispatch({
      type: "CART_ADD_ITEM",
      payload: { ...item, quantity },
    });
  };

  const removeItemHandler = (item) => {
    ctxDispatch({ type: "CART_REMOVE_ITEM", payload: item });
  };

  const checkoutHandler = () => {
    navigate("/signin?redirect=/shipping");
  };
  return (
    <div>
      <Helmet>
        <title>Shopping Cart</title>
      </Helmet>
      <h1 class="pageTitle">Cart</h1>
      {cartItems.length === 0 ? (
        <MessageBox>
          Cart is empty. <Link to="/">Go Shopping</Link>
        </MessageBox>
      ) : (
        <ListGroup>
          {cartItems.map((item) => (
            <ListGroup.Item key={item._id}>
              <Row className="align-items-center">
                <img
                  src={item.image}
                  alt={item.name}
                  className="img-fluis rounded img-thumbnail"
                ></img>{" "}
                <Link to={`/product/${item.slug}`}>{item.name}</Link>
                <Button
                  variant="light"
                  onClick={() => updateCartHandler(item, item.quantity - 1)}
                  disabled={item.quantity === 1}
                >
                  <i className="fas fa-minus-circle"></i>
                </Button>{" "}
                <span>{item.quantity}</span>{" "}
                <Button
                  variant="light"
                  onClick={() => updateCartHandler(item, item.quantity + 1)}
                  disabled={item.quantity === item.countInStock}
                >
                  <i className="fas fa-plus-circle"></i>
                </Button>{" "}
                ${item.price}
                <Button onClick={() => removeItemHandler(item)} variant="light">
                  <i className="fas fa-trash"></i>
                </Button>
              </Row>
            </ListGroup.Item>
          ))}
        </ListGroup>
      )}
      <ListGroup variant="flush">
        <ListGroup.Item>
          <h3>
            Subtotal({cartItems.reduce((a, c) => a + c.quantity, 0)} items) : $
            {cartItems.reduce((a, c) => a + c.price * c.quantity, 0)}
          </h3>
        </ListGroup.Item>
        <ListGroup.Item>
          <Button
            variant="primary"
            onClick={checkoutHandler}
            disabled={cartItems.length === 0}
          >
            Proceed to Checkout
          </Button>
        </ListGroup.Item>
      </ListGroup>
    </div>
  );
}
