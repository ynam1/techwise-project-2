import { Link } from 'react-router-dom';
function NavBar() {
  return (
    <div>
      <link
        rel="stylesheet"
        type="text/css"
        href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css"
      ></link>
      <footer class="footer">
        <div class="container">
          <div class="row">
            <div class="footer-col">
              <section class="company">
                <h4>TYSM</h4>
                <ul>
                  <li>
                    <Link to="./about.html">About Us</Link>
                  </li>
                  <li>
                    <Link to="./wishlist.html">Wishlist</Link>
                  </li>
                  <li>
                    <Link to="/">Share</Link>
                  </li>
                  <li>
                    <Link to="/">Privacy Policy</Link>
                  </li>
                </ul>
              </section>
            </div>
            <div class="footer-col">
              <section class="help">
                <h4>HELP</h4>
                <ul>
                  <li>
                    <Link to="/">FAQ</Link>
                  </li>
                  <li>
                    <Link to="/">Shipping</Link>
                  </li>
                  <li>
                    <Link to="/">Returns</Link>
                  </li>
                  <li>
                    <Link to="/">Order Status</Link>
                  </li>
                  <li>
                    <Link to="/">Billing</Link>
                  </li>
                </ul>
              </section>
            </div>
            <div class="footer-col">
              <section class="follow-us">
                <h4>FOLLOW US</h4>
                <div class="social-links">
                  <ul>
                    <Link to="/">
                      <i class="fab fa-facebook-f"></i>
                    </Link>
                    <Link to="/">
                      <i class="fab fa-twitter"></i>
                    </Link>
                    <Link to="/">
                      <i class="fab fa-instagram"></i>
                    </Link>
                    <Link to="/">
                      <i class="fab fa-linkedin-in"></i>
                    </Link>
                  </ul>
                </div>
              </section>
            </div>
          </div>
        </div>
      </footer>
    </div>
  );
}
export default NavBar;
